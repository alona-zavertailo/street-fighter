import App from './src/javascript/app';
import './src/styles/style.css';
new App();

class Fighter {
    constructor(name, health, attack, defense) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
      }
    getHitPower(attack) {
        const min = 1;
        const max = 2;
        let criticalHitChance = Math.random() * (+max - +min) + +min; 
        this.power = attack * criticalHitChance;
    }
    getBlockPower(defense) {
        const min = 1;
        const max = 2;
        let dodgeChance = Math.random() * (+max - +min) + +min; 
        this.power = defense * dodgeChance;
    }
    
}
let fighter = new Fighter();
fighter.getHitPower(); 
fighter.getBlockPower();

function fight(play) {
    health -= (fighter.getHitPower - fighter.getBlockPower);
}